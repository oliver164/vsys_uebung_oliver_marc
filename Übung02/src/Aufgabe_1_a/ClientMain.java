package Aufgabe_1_a;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class ClientMain {
	private static final int port=1234;
	private static final String hostname="localhost";
	private static MySocketClient client;
	private static BufferedReader reader;
	private static boolean exitKey = false;
	private static int sleepTime = 1000;

	public static void main(String args[]) {
		try {
			client=new MySocketClient(hostname,port);

			Thread requestHandler = new Thread(() -> {
				while(!exitKey) {
					try {
						sleepTime = (int)(Math.random()*3000);
						client.sendAndReceive();
						Thread.sleep(sleepTime);
					} catch (Exception var1) {
						System.out.println("!!!!!!!!!!! ERROR: " + var1.getMessage());
						var1.printStackTrace();
					}
				}

			});
			requestHandler.start();
			reader=new BufferedReader(new InputStreamReader(System.in));
			System.out.print("Press key to exit");
			if (reader.readLine() != null) {
				exitKey = true;
			}
			requestHandler.join();
			client.disconnect();
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}
}
