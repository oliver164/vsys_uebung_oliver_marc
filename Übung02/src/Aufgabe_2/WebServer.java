package Aufgabe_2;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.StringTokenizer;
import java.util.logging.FileHandler;
import java.util.logging.Logger;

public class WebServer implements Runnable {
    static final File WEB_ROOT = new File("././.");
    static final String DEFAULT_FILE = "website.html";
    static final String FILE_NOT_FOUND = "404.html";
    static final int PORT = 8080;
    private Socket socket;
    private static Logger logger = Logger.getLogger("MyLog");
    private static FileHandler fileHandler;

    public WebServer(Socket socket) {
        this.socket = socket;
    }

    public static void main(String[] args) {

        String fileName = null;
        Integer port = null;

        try {
            for(int i = 0; i < args.length; ++i) {
                if (args[i].matches("^-port$")) {
                    port = Integer.valueOf(args[i + 1]);
                }
                if (args[i].matches("^-log$")) {
                    fileName = args[i + 1];
                }
            }

            try {
                fileHandler = new FileHandler("./" + fileName + ".log");
                logger.addHandler(fileHandler);
            } catch (IOException ioException) {
                System.err.println("Error while creating Log File");
            }

            ServerSocket serverConnect = new ServerSocket(port);

            while(true) {
                WebServer myServer = new WebServer(serverConnect.accept());
                Thread clientConnection = new Thread(myServer);
                clientConnection.start();
            }
        } catch (IOException ioException) {
            System.err.println("Server Connection lost");
        }
    }

    public void run() {
        BufferedReader fromClientSocket = null;
        PrintWriter toClientSocket = null;
        BufferedOutputStream requestedToClient = null;
        File file = null;
        try {
            logger.info("Setting up streams\n");
            fromClientSocket = new BufferedReader(new InputStreamReader(this.socket.getInputStream()));
            toClientSocket = new PrintWriter(this.socket.getOutputStream());
            requestedToClient = new BufferedOutputStream(this.socket.getOutputStream());

            logger.info("Getting input from client via socket and process given data\n");
            String inputClientViaSocket = fromClientSocket.readLine();

            if (inputClientViaSocket != null) {
                StringTokenizer stringTokenizer = new StringTokenizer(inputClientViaSocket);
                String methodRequested = stringTokenizer.nextToken().toUpperCase(); // to upper case notwendig?
                String fileRequested = stringTokenizer.nextToken().toLowerCase();

                logger.info("search for file requested: " + fileRequested + "\n");
                if(fileRequested.endsWith("/")){
                   file = new File(WEB_ROOT,fileRequested + DEFAULT_FILE);
                }else{
                   file =  new File(WEB_ROOT,fileRequested);
                }

                int fileLength = (int)file.length();
                System.out.println(file);

                if (methodRequested.equals("GET")) {
                    byte[] fileData = this.readFileData(file, fileLength);

                    logger.info("send header back \n");
                    toClientSocket.println("HTTP/1.1 200 OK\r\n");
                    toClientSocket.flush();

                    logger.info("send html back \n");
                    requestedToClient.write(fileData, 0, fileLength);
                    requestedToClient.flush();

                    logger.info("close all streams \n");
                    fromClientSocket.close();
                    toClientSocket.close();
                    requestedToClient.close();
                }

                return;
            }

            this.socket.close();
            logger.info("empty streams so connection closed\n");
        } catch (FileNotFoundException var22) {
            fileNotFound(toClientSocket,requestedToClient);            return;
        } catch (IOException var23) {
            var23.printStackTrace();
            return;
        } finally {
            try {
                logger.info("connection closed\n");
                this.socket.close();
            } catch (Exception var21) {
                logger.warning("Error closing stream : " + var21.getMessage() + "\n");
                System.err.println("Error closing stream : " + var21.getMessage());
            }

        }

    }

    private byte[] readFileData(File file, int fileLength) throws IOException {
        logger.info("read File Data " + file.getAbsolutePath() + " \n");
        FileInputStream fileIn = null;
        byte[] fileData = new byte[fileLength];

        try {
            fileIn = new FileInputStream(file);
            fileIn.read(fileData);
        } finally {
            if (fileIn != null) {
                fileIn.close();
            }

        }

        return fileData;
    }

    private void fileNotFound(PrintWriter toClientSocket, BufferedOutputStream requestedToClient) {
        try {
            logger.warning("the file requested wasn't found. Return 202'!\n");
            File file = new File(WEB_ROOT, FILE_NOT_FOUND);
            int fileLength = (int)file.length();
            byte[] fileData = this.readFileData(file, fileLength);
            toClientSocket.println("HTTP/1.1 404 NOT_FOUND\r\n");
            toClientSocket.flush();
            requestedToClient.write(fileData, 0, fileLength);
            requestedToClient.flush();
        } catch (IOException var7) {
            logger.warning("the file with info 'file not found' wasn't found\n");
            var7.printStackTrace();
        }

    }
}
