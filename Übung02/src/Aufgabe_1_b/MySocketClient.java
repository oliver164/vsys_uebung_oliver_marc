package Aufgabe_1_b;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
 
public class MySocketClient {
	private Socket socket;
	private ObjectInputStream objectInputStream;
	private ObjectOutputStream objectOutputStream;
	private String clientName = null;
	private int counter = 0;
	
	MySocketClient(String hostname, int port, String clientName)
					throws IOException {
		this.clientName = clientName;
		socket=new Socket();
		System.out.print("Client: connecting '"+hostname+
			"' on "+port+" ... ");
		socket.connect(new InetSocketAddress(hostname,port));
		System.out.println("done.");
		objectInputStream=
			new ObjectInputStream(socket.getInputStream());
		objectOutputStream=
			new ObjectOutputStream(socket.getOutputStream());
	}

	public String sendAndReceive() throws Exception {
		this.objectOutputStream.writeObject(this.clientName+"Send: Test-" + this.counter);

		System.out.println("Client"+this.clientName+": send Test-" + this.counter++);
		return (String)this.objectInputStream.readObject();
	}
	
	public void disconnect() 
					throws IOException {
		try {
			socket.close();
		}
		catch(IOException e) {
			e.printStackTrace();
		}
	}
}
