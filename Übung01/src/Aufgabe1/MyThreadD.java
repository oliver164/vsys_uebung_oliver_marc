package Aufgabe1;

public class MyThreadD extends MyAccount implements Runnable {
    private static final int threadMax = 10;

    public static void main(String args[]) {
        MyThreadD account = new MyThreadD();
        for (int i = 0; i < threadMax; i++) {
            new Thread(account).start();
        }
    }

    public synchronized void run() {
        int var = this.getX();

        System.out.print(Thread.currentThread().getName()+" : "+var);
        if (Math.random()>=0.5) {
            System.out.print(" + 1 ");
            this.setX(var+1);
            System.out.println(" = " + (var+1));
        } else {
            System.out.print(" - 1 ");
            this.setX(var-1);
            System.out.println(" = " + (var-1));
        }

    }
}
