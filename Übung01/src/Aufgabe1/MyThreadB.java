package Aufgabe1;

public class MyThreadB extends Thread {
    private static final int threadMax=10;
    private static int runCount=0;

    public void run() {
        synchronized (Thread.currentThread()){
            while(runCount++<100) {
                synchronizedThreads();
            }
        }
    }

    public synchronized void synchronizedThreads() {
        System.out.println(runCount+": "+Thread.currentThread().getName());
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    public static void main(String args[]) {
        for(int i=0;i<threadMax;i++) {
            new MyThreadB().start();
        }
    }
}
