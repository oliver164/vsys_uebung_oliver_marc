package Aufgabe1;

public class MyAccount {
    private static int x = 5;

    public synchronized static void setX(int x) {
        MyAccount.x = x;
    }

    public synchronized static int getX() {
        return x;
    }
}
