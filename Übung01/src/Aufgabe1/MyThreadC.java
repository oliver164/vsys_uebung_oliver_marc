package Aufgabe1;

public class MyThreadC extends Thread {
    private static final int threadMax = 10;
    private static Integer runCount = 0;

    public static void main(String args[]) {
        for (int i = 0; i < threadMax; i++) {
            new MyThreadC().start();
        }
    }

    public void run() {
        while (runCount++ < 100) {
            synchronizedThreads();
        }
    }

   public static synchronized void synchronizedThreads() { // static synchronized sorgt dafür, dass solange ein Thread in der Methode ist, diese für alle anderen geblockt ist
       System.out.println(runCount+": "+Thread.currentThread().getName());
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}