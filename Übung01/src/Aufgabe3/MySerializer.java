package Aufgabe3;

import java.io.*;

public class MySerializer {
	private MySerializableClass mySerializableClass;
	
	MySerializer(MySerializableClass serializableClass) {
		mySerializableClass=serializableClass;
	}
	
	private String readFilename() throws IOException {
		String filename;
		BufferedReader reader=new BufferedReader(new InputStreamReader(System.in )); 
		
		System.out.print("filename> ");
		filename=reader.readLine();
		
		return filename;
	}
	
	public void write(String text) throws IOException {
		mySerializableClass.set(text);
		String filename=readFilename();
			FileOutputStream fileStream = new FileOutputStream(filename);
			ObjectOutputStream outStream = new ObjectOutputStream(fileStream);
			outStream.writeObject(mySerializableClass);
			outStream.close();
	}
	
	public String read() throws IOException, ClassNotFoundException {
		String filename=readFilename();
			FileInputStream fileStream = new FileInputStream(filename);
			ObjectInputStream input = new ObjectInputStream(fileStream);
			mySerializableClass = (MySerializableClass) new ObjectInputStream(new FileInputStream(filename)).readObject();
			input.close();
		return mySerializableClass.toString();
	}
} 
	