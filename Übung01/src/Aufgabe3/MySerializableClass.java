package Aufgabe3;

import java.io.Serializable;

public class MySerializableClass implements Serializable{
	private static final long serialVersionUID=1;
	private int id;
	private String string;

	transient private MyNonSerializableClass nonSerializable;

	MySerializableClass() {
		id=1234;
		nonSerializable = new MyNonSerializableClass();
	}
	
	public void set(String string) {
		this.string=string;
	}

	public String toString() {

		return "id: "+id+"; string: "+string+ " nonSer: "+ nonSerializable;
	}
} 
	