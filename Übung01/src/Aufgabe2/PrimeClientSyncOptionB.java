package Aufgabe2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import rm.requestResponse.*;

public class PrimeClientSyncOptionB extends Thread {
    private static final String HOSTNAME="localhost";
    private static final int PORT=1234;
    private static final long INITIAL_VALUE=(long)1e17;
    private static final Long COUNT = Long.valueOf(20);
    private static final String CLIENT_NAME=PrimeClient.class.getName();
    private static final Boolean REQUEST_MODE = true;
    private final Boolean requestMode;
    private static long i = 0;

    private Component communication;
    String hostname;
    int port;
    long initialValue,count;

    public PrimeClientSyncOptionB(String hostname, int port, long initialValue, long count, Boolean requestMode) {
        this.hostname=hostname;
        this.port=port;
        this.initialValue=initialValue;
        this.count=count;
        this.requestMode = requestMode;
    }

    public void run() {
        communication=new Component();
        for (long i=initialValue;i<initialValue+count;i++) {
            try {
                processNumber();
            } catch (IOException e) {
                throw new RuntimeException(e);
            } catch (ClassNotFoundException e) {
                throw new RuntimeException(e);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public void processNumber() throws IOException, ClassNotFoundException, InterruptedException {
        synchronized (COUNT) {
            if (i >= initialValue + count) return;
            communication.send(new Message(hostname, port, new Long(i)), false);
            Boolean isPrime = (Boolean) communication.receive(port, true, true).getContent();
            System.out.println(Thread.currentThread().getName()+" : "+i + ": " + (isPrime.booleanValue() ? "prime" : "not prime"));
            i++;
        }
    }

    public static void main(String args[]) throws IOException, ClassNotFoundException {
        String hostname=HOSTNAME;
        int port=PORT;
        long initialValue=INITIAL_VALUE;
        long count=COUNT;
        boolean requestMode = REQUEST_MODE;
        boolean doExit=false;

        String input;
        BufferedReader reader=new BufferedReader(new InputStreamReader(System.in ));

        System.out.println("Welcome to "+CLIENT_NAME+"\n");

        while(!doExit) {
            System.out.print("Server hostname ["+hostname+"] > ");
            input=reader.readLine();
            if(!input.equals("")) hostname=input;

            System.out.print("Server port ["+port+"] > ");
            input=reader.readLine();
            if(!input.equals("")) port=Integer.parseInt(input);

            System.out.print("Request mode synchronized? [" + requestMode + "] > ");
            input = reader.readLine();
            if (input.equals("false")) requestMode = false;

            System.out.print("Prime search initial value ["+initialValue+"] > ");
            input=reader.readLine();
            if(!input.equals("")) initialValue=Integer.parseInt(input);

            System.out.print("Prime search count ["+count+"] > ");
            input=reader.readLine();
            if(!input.equals("")) count=Integer.parseInt(input);

            if (!requestMode) {
                new PrimeClientSyncOptionB(hostname, port, initialValue, count, requestMode).run();
            } else {
                for (int i = 0; i < 4; i++) {
                    new PrimeClientSyncOptionB(hostname, port, initialValue, count, requestMode).start();
                }
            }

            System.out.println("Exit [n]> ");
            input=reader.readLine();
            if(input.equals("y") || input.equals("j")) doExit=true;
        }
    }
}

