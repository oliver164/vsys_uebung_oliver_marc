package Aufgabe2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import rm.requestResponse.*;


public class PrimeClient_C_and_D extends Thread {
    private static final String HOSTNAME="localhost";
    private static final int PORT=1234;
    private static final long INITIAL_VALUE=(long)1e17;
    private static final Long COUNT = Long.valueOf(20);
    private static final String CLIENT_NAME= PrimeClient_C_and_D.class.getName();
    private static final int REQUEST_MODE_C = 1;
    private final int requestMode_C;
    private static long i = 0;

    private Component communication;
    String hostname;
    int port;
    long initialValue,count;

    public PrimeClient_C_and_D(String hostname, int port, long initialValue, long count, int requestMode_C) {
        this.hostname=hostname;
        this.port=port;
        this.initialValue=initialValue;
        this.count=count;
        this.requestMode_C = requestMode_C;
    }

    public void run() {
        communication=new Component();
        for (long i=initialValue;i<initialValue+count;i++) {
            try {
                pollingCounter();
            } catch (IOException e) {
                throw new RuntimeException(e);
            } catch (ClassNotFoundException e) {
                throw new RuntimeException(e);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public void pollingCounter() throws IOException, ClassNotFoundException, InterruptedException {
        synchronized (COUNT) {
            if (i >= initialValue + count) return;
            communication.send(new Message(hostname, port, new Long(i)), false);
            Boolean isPrime = null;

            if (requestMode_C == 3 || requestMode_C == 4)
                isPrime = receivePolling();
            else
                isPrime = (Boolean) communication.receive(port, true, true).getContent();


            System.out.println(Thread.currentThread().getName()+" : "+i + ": " + (isPrime.booleanValue() ? "prime" : "not prime"));
            i++;
        }
    }

    private boolean receivePolling() throws IOException, ClassNotFoundException {
        Message returnedMessage = communication.receive(port, false, true);
        if (returnedMessage != null)
            return (Boolean) returnedMessage.getContent();
        try {
            Thread.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.print(".");
        return receivePolling();
    }

    public static void main(String args[]) throws IOException, ClassNotFoundException {
        String hostname=HOSTNAME;
        int port=PORT;
        long initialValue=INITIAL_VALUE;
        long count=COUNT;
        int requestMode_C = REQUEST_MODE_C;
        boolean doExit=false;

        String input;
        BufferedReader reader=new BufferedReader(new InputStreamReader(System.in ));

        System.out.println("Welcome to "+CLIENT_NAME+"\n");

        while(!doExit) {
            System.out.print("Server hostname ["+hostname+"] > ");
            input=reader.readLine();
            if(!input.equals("")) hostname=input;

            System.out.print("Server port ["+port+"] > ");
            input=reader.readLine();
            if(!input.equals("")) port=Integer.parseInt(input);

            System.out.print("Request mode 1:asynchron 2:synchron 3:polling 4:async polling [" + requestMode_C + "] > ");
            input = reader.readLine();
            if (input.equals("1")) {requestMode_C = 1;}
            else if (input.equals("2")){requestMode_C =2;}
            else if (input.equals("3")){requestMode_C =3;}
            else if (input.equals("4")){requestMode_C =4;}

            System.out.print("Prime search initial value ["+initialValue+"] > ");
            input=reader.readLine();
            if(!input.equals("")) initialValue=Integer.parseInt(input);

            System.out.print("Prime search count ["+count+"] > ");
            input=reader.readLine();
            if(!input.equals("")) count=Integer.parseInt(input);

            if (requestMode_C==1) {
                new PrimeClient_C_and_D(hostname, port, initialValue, count, requestMode_C).run();
            } else if (requestMode_C==2 || requestMode_C == 4){
                for (int i = 0; i < 4; i++) {
                    new PrimeClient_C_and_D(hostname, port, initialValue, count, requestMode_C).start();
                }
            }else{
                new PrimeClient_C_and_D(hostname, port, initialValue, count, requestMode_C).run();
            }

            System.out.println("Exit [n]> ");
            input=reader.readLine();
            if(input.equals("y") || input.equals("j")) doExit=true;
        }
    }
}

