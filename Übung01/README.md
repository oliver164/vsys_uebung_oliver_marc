# Übung 01

### Aufgabe 1
#### a) Starten Sie die Ausführung dieses Programms. Was macht das Programm und was fällt Ihnen auf?

- Das Programm erstellt in der Main Methode 10 Threads die gestartet werden und somit jeweils die run() Methode aufrufen
- In der Run Methode wird in einer While Schleife die Variable runCount bis 100 inkrementiert und in jedem Durchlauf die aktulle Stelle mit dem jeweilig arbeitenden Thread ausgegeben
- In manchen Momenten gibt es mehrere Ausgaben vom gleich Thread hintereinander, manchmal ist es durcheinander
- Reihenfolge der Zahlen nicht korrekt

#### b) Warum erfolgt die Ausgabe der Threads trotz Synchronisierung immer noch nebenläufig?

- Wir haben in dem Programm keine konkurrierenden Threads. Sie müssen nicht aufeinander warten.
- Dies ist der fall, da wir mit synchronized die objekte blockieren und nicht die klassen. Hierüfr müsste static im Rumpf ergänzt werden.


### Aufgabe 2

#### a) Bei hohen Startwerten erfolgt teilweise lange keine Rückmeldung des Clients im Sinneeiner Ausgabe an den Anwender. Dies hat mindestens drei verschiedene Ursachen. Welche sind das?

- um Primzahlen zu berechnen, muss der PrimeServer alle niedrigeren Zahlen mit modulo testen
- da in der Standard PrimeClient nur ein Thread arbeitet

#### e) Testen Sie das Szenario mit mehreren nebenläufigen Clients und nennen Sie mögliche Gründe für die Ursachen des gezeigten Verhaltens.

- Wenn ein Client die Connection aufbaut während der andere diese gerade schließt entsteht eine SocketException und er muss die Connection neu aufbauen
- Oder da wir den gleichen Port benutzen kann sich immernur ein Client connecten